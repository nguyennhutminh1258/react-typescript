// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDlsZ1rc8L2eEk2qhasH4HiyUcxsrq3D-0",
  authDomain: "userdata-6974e.firebaseapp.com",
  projectId: "userdata-6974e",
  storageBucket: "userdata-6974e.appspot.com",
  messagingSenderId: "795682008953",
  appId: "1:795682008953:web:d0cd80df20f05a689d89a8",
  measurementId: "G-363BG7TE07",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
