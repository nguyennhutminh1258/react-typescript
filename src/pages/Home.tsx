import Button from "../components/Button";
import InputUser from "../components/InputUser";
import ListUser from "../components/ListUser";

const Home = () => {
  return (
    <div className="container">
      <InputUser />
      <Button style={{ backgroundColor: "red", padding: "20px" }} />
      <div>
        <ListUser>
          <div className="title-container">
            <span className="title">Danh sách người dùng</span>
          </div>
        </ListUser>
      </div>
    </div>
  );
};

export default Home;
