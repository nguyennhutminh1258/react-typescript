interface IUser {
  id: number;
  name?: string;
}

type UserState = {
  users: IUser[];
};

type UserAction = {
  type: string;
  user: IUser;
};

