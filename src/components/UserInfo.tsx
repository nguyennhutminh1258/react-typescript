import { FC, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { editUser, removeUser } from "../store/actions/userCreators";
import { Dispatch } from "redux";

interface UserInfoProps {
  user: {
    id: number;
    name?: string;
  };
}

const UserInfo: FC<UserInfoProps> = (props) => {
  const dispatch: Dispatch<any> = useDispatch();
  const [onEdit, setOnEdit] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleEditUser = () => {
    if (props.user.name !== inputRef.current?.value) {
      const newUser = { ...props.user, name: inputRef.current?.value };
      dispatch(editUser(newUser));
    }
    setOnEdit(false);
  };

  const handleDeleteUser = () => {
    dispatch(removeUser(props.user));
  };

  return (
    <div className="list-user-item">
      <div className="user-profile">
        {onEdit ? (
          <input
            className="user-edit-input"
            type="text"
            defaultValue={props.user?.name}
            ref={inputRef}
          />
        ) : (
          <span>{props.user?.name}</span>
        )}
      </div>
      <div className="group-button">
        <button
          className="edit-button"
          onClick={() => (onEdit ? handleEditUser() : setOnEdit(true))}>
          {onEdit ? " Lưu" : "Sửa"}
        </button>

        <button
          className="delete-button"
          onClick={() => (onEdit ? setOnEdit(false) : handleDeleteUser())}>
          {onEdit ? "Hủy" : "Xóa"}
        </button>
      </div>
    </div>
  );
};

export default UserInfo;
