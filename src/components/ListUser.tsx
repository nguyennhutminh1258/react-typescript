import { FC, ReactNode } from "react";
import { useSelector } from "react-redux";
import UserInfo from "./UserInfo";

interface ListUserProps {
  children: ReactNode;
}

const ListUser: FC<ListUserProps> = ({ children }) => {
  const users: readonly IUser[] = useSelector((state: UserState) => state.users);

  return (
    <div>
      {children}
      <div className="list-user">
        {users.map((user) => {
          return <UserInfo user={user} key={user?.id} />;
        })}
      </div>
    </div>
  );
};

export default ListUser;
