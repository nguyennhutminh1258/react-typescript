import { CSSProperties } from "react";

interface IButtonProps {
  style: CSSProperties;
}

const Button = ({ style }: IButtonProps) => {
  return (
    <div>
      <button style={style}>Button</button>
    </div>
  );
};

export default Button;
