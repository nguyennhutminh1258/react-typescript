import { FC, useRef } from "react";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { addUser } from "../store/actions/userCreators";

const InputUser: FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const inputRef = useRef<HTMLInputElement>(null);

  const addNewUser = () => {
    dispatch(addUser({ id: Math.random(), name: inputRef.current?.value }));
    inputRef.current!.value = ""; // Using the non-null assertion operator to solve the error
  };

  return (
    <div className="input-user-container">
      <input
        className="input-user"
        type="text"
        ref={inputRef}
        placeholder="Nhập tên người dùng..."
      />
      <button className="add-button" onClick={addNewUser}>
        Thêm
      </button>
    </div>
  );
};

export default InputUser;
