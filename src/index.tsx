import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { createStore, applyMiddleware, Store, Dispatch, Action } from "redux";
import { Provider } from "react-redux";
import userReducer from "./store/reducers/userReducer";
import thunk from "redux-thunk";
import { BrowserRouter } from "react-router-dom";

const store: Store<UserState, Action> & {
  dispatch: Dispatch;
} = createStore(userReducer, applyMiddleware(thunk));

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
 <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
 </BrowserRouter>
);
