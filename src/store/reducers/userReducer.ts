import * as actionTypes from "./../actions/userTypes";

interface GetUserAction {
  type: "GET_USER";
  users: IUser[];
}

interface AddUserAction {
  type: "ADD_USER";
  user: IUser;
}

interface EditUserAction {
  type: "EDIT_USER";
  user: IUser;
}

interface RemoveUserAction {
  type: "REMOVE_USER";
  user: IUser;
}

type Action = GetUserAction | AddUserAction | EditUserAction | RemoveUserAction;

const initialState: UserState = {
  users: [],
};

const userReducer = (state: UserState = initialState, action: Action): UserState => {
  switch (action.type) {
    case actionTypes.GET_USER:
      return {
        ...state,
        users: action.users,
      };

    case actionTypes.ADD_USER:
      return {
        ...state,
        users: state.users.concat(action.user),
      };

    case actionTypes.EDIT_USER:
      const newUsers: IUser[] = state.users.map((user) =>
        user.id === action.user.id ? action.user : user
      );

      return {
        ...state,
        users: newUsers,
      };

    case actionTypes.REMOVE_USER:
      const updatedUsers: IUser[] = state.users.filter((user) => user.id !== action.user.id);
      return {
        ...state,
        users: updatedUsers,
      };

    default:
      return state;
  }
};

export default userReducer;
