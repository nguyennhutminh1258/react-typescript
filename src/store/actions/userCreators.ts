import { collection, query, getDocs, addDoc, doc, updateDoc, deleteDoc } from "firebase/firestore";
import { Dispatch } from "redux";
import { db } from "../../firebaseConfig";
import * as actionTypes from "./userTypes";

export const getUser = () => async (dispatch: Dispatch) => {
  const userRef = collection(db, "user"); // Tham chiếu đến vị trí collection
  const q = query(userRef);
  const docsSnap = await getDocs(q); // Dùng hàm getDocs để lấy document từ collection
  const users: IUser[] = [];
  // Lặp qua docsSnap để lấy document đưa vào mảng users
  docsSnap.forEach((docItem) => {
    users.push({ id: docItem.data().id, name: docItem.data().name });
  });

  return dispatch({
    type: actionTypes.GET_USER,
    users,
  });
};

export const addUser = (user: IUser) => async (dispatch: Dispatch) => {
  await addDoc(collection(db, "user"), user);

  return dispatch({
    type: actionTypes.ADD_USER,
    user,
  });
};

export const editUser = (user: IUser) => async (dispatch: Dispatch) => {
  const userRef = collection(db, "user");
  const q = query(userRef);
  const docsSnap = await getDocs(q);

  docsSnap.forEach(async (docItem) => {
    if (docItem.data().id === user.id) {
      await updateDoc(doc(db, "user", docItem.id), {
        name: user.name,
      });
    }
  });

  return dispatch({
    type: actionTypes.EDIT_USER,
    user,
  });
};

export const removeUser = (user: IUser) => async (dispatch: Dispatch) => {
  const userRef = collection(db, "user");
  const q = query(userRef);
  const docsSnap = await getDocs(q);

  docsSnap.forEach(async (docItem) => {
    if (docItem.data().id === user.id) {
      await deleteDoc(doc(db, "user", docItem.id));
    }
  });

  return dispatch({
    type: actionTypes.REMOVE_USER,
    user,
  });
};
